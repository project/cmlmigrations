<?php

namespace Drupal\cmlmigrations\Service;

use Drupal\cmlapi\Service\Scheme as CmlScheme;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Scheme Migrations not ready.
 */
class Scheme {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new Pileline manager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\cmlapi\Service\Scheme $cml_scheme
   *   The cml service.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      CmlScheme $cml_scheme
    ) {
    // $this->cmlScheme = $cml_scheme;
    // $this->configFactory = $config_factory;
    // $this->extetnal = new External();
  }

  /**
   * Init.
   */
  public function init(int $cid) {
    // $scheme = $this->cmlScheme->init($cid);
    // $scheme = $this->extetnal->scheme();
    // return $scheme;
  }

  /**
   * Terms.
   */
  public function terms() : array {
    // $terms = $this->extetnal->outTerms();
    return [];
  }

  /**
   * Vocabularies.
   */
  public function vocabularies() : array {
    // $terms = $this->extetnal->outVocabularies();
    return [];
  }

  /**
   * Product Types.
   */
  public function productTypes() : array {
    // $terms = $this->extetnal->outProductTypes();
    return [];
  }

}
