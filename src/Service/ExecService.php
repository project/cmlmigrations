<?php

namespace Drupal\cmlmigrations\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Exec Service.
 */
class ExecService {

  //phpcs:ignore
  protected ConfigFactoryInterface $configFactory;

  /**
   * Creates a new Exec Service manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Exec.
   */
  public function exec($nohup = TRUE, $update = FALSE) {
    $drush = $this->getDrush();
    $root = DRUPAL_ROOT;
    $cmd = "{$drush} mim --group=cml --root=$root";
    if ($update) {
      $cmd .= " --update";
    }
    if ($nohup) {
      $filename = 'cmlmigrations-nohup';
      $cmd = "nohup $cmd > ~/$filename.log 2> ~/$filename-err.log < /dev/null &";
    }
    $result = "<b>\$ $cmd</b>\n";
    $result .= shell_exec($cmd);
    return $result;
  }

  /**
   * Drush nohup run cron.
   */
  public function nohupRunMigrations() : string {
    $drush = $this->getDrush();
    $filename = 'cmlmigrations.log';
    $cmd = "nohup $drush cmlmigrations >> ~/$filename &";
    $result = "<b>\$ $cmd</b>\n";
    $result .= shell_exec($cmd);
    return $result;
  }

  /**
   * Exec test.
   */
  public function execTest() {
    $cmd = "whoami";
    $result = "<b>\$ $cmd</b>\n";
    $result .= shell_exec($cmd);
    return $result;
  }

  /**
   * Drush test.
   */
  public function drushTest() {
    $drush = $this->getDrush();
    $cmd = "{$drush} --version";
    $result = "<b>\$ $cmd</b>\n";
    $result .= shell_exec($cmd);
    return $result;
  }

  /**
   * Drush test.
   */
  public function nohupTest() {
    $cmd = "nohup --version";
    $result = "<b>\$ $cmd</b>\n";
    $result .= shell_exec($cmd);
    return $result;
  }

  /**
   * Drush.
   */
  public function getDrush() {
    $config = $this->configFactory->get('cmlmigrations.settings');
    $drush = "/var/www/html/vendor/bin/drush";
    if ($config->get('drush')) {
      $drush = $config->get('drush');
    }
    return $drush;
  }

}
