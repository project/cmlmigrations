<?php

namespace Drupal\cmlmigrations\Service;

/**
 * Migrate Service.
 */
class MigrateService {

  /**
   * Get migrations.
   */
  public static function getCmlGroup() : array {
    $migrations = [];
    $manager = FALSE;
    try {
      $manager = \Drupal::service('plugin.manager.migration');
    }
    catch (\Exception $e) {
      return FALSE;
    }
    if ($manager) {
      $plugins = $manager->createInstances([]);
      if (!empty($plugins)) {
        $migrations['status'] = TRUE;
        foreach ($plugins as $id => $migration) {
          if ($migration->migration_group == 'cml') {
            $source_plugin = $migration->getSourcePlugin();
            $map = $migration->getIdMap();
            if ($migration->getStatusLabel() != 'Idle') {
              $migrations['status'] = FALSE;
            }
            $migrations['list'][$id] = [
              'id' => $migration->id(),
              'label' => $migration->label(),
              'group' => $migration->migration_group,
              'status' => $migration->getStatusLabel(),
              'total' => $source_plugin->count(),
              'imported' => (int) $map->importedCount(),
              'unprocessed' => $source_plugin->count() - (int) $map->importedCount(),
              'messages' => $map->messageCount(),
              'last' => \Drupal::keyValue('migrate_last_imported')->get($migration->id(), FALSE),
            ];
          }
        }
      }
    }
    return $migrations;
  }

  /**
   * Hook.
   */
  public static function import() {
    $config = \Drupal::service('config.factory')->getEditable('cmlapi.settings');
    while (TRUE) {
      \Drupal::state()->set('cml_migrations', time());
      if (empty(\Drupal::service('cmlapi.cml')->query(1))) {
        \Drupal::state()->set('cml_migrate_current', FALSE);
        break;
      }
      $migrations = \Drupal::service('cmlmigrations.migrate')->getCmlGroup();
      $idle = TRUE;
      $last = 0;
      foreach ($migrations['list'] as $id => $migration) {
        if ($migration['status'] != 'Idle') {
          $idle = FALSE;
          $last = $migration['last'];
        }
      }
      if ($idle) {
        \Drupal::logger(__CLASS__)->warning('process migration is free');
        $progress = \Drupal::service('cmlmigrations.pipeline')->query('progress');
        if (!empty($progress)) {
          $result = \Drupal::service('cmlmigrations.pipeline')->import($progress, $migrations);
          if ($result == 'success') {
            $config->set('runing_cml', '')->save();
            \Drupal::state()->set('cml_migrate_current', FALSE);
          }
        }
        else {
          $cml = \Drupal::service('cmlapi.cml')->actual();
          if ($cml) {
            $status = $cml->getState();
            $config->set('runing_cml', $cml->id())->save();
            \Drupal::state()->set('cml_migrate_current', [$cml->id() => time()]);
            $result = \Drupal::service('cmlmigrations.pipeline')->import($cml, $migrations);
          }
        }
      }
      else {
        \Drupal::logger(__CLASS__)->warning('process migration is busy');
      }
      \Drupal::logger(__CLASS__)->warning('sleeping ... 3 sec');
      sleep(3);
    }
    \Drupal::state()->set('cml_migrations', FALSE);
  }

}
