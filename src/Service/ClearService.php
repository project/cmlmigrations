<?php

namespace Drupal\cmlmigrations\Service;

/**
 * Clear Service.
 */
class ClearService {

  /**
   * Migration plugin manager service.
   */
  protected object $migrationPluginManager;

  /**
   * Construct a new ClearService.
   */
  public function __construct() {
    $this->migrationPluginManager = \Drupal::service('plugin.manager.migration');
  }

  /**
   * Debug.
   */
  public function debug() {

  }

  /**
   * Clear.
   */
  public function clear() : array {
    $result = [
      'migrations' => $this->clearMigrations(),
      'cmlmigrations' => $this->clearCmlmigrations(),
    ];
    return $result;
  }

  /**
   * Clear Migrations.
   */
  private function clearMigrations() : bool {
    $state = \Drupal::state()->get('cml_migrate_current');
    if ($state) {
      $ids = array_keys($state);
      $cml_id = reset($ids);
      $cml_time = reset($state);
      // Checking how much time has passed since the last cml.
      if (time() - $cml_time > 3600) {
        $config = \Drupal::service('config.factory')->getEditable('cmlapi.settings');
        $config->set('runing_cml', '')->save();
        \Drupal::state()->set('cml_migrate_current', FALSE);
        $this->resetStatusMigrations();
        \Drupal::logger(__CLASS__)->notice("Reset the status to Idle for all migrations.");
        $cml = \Drupal::entityTypeManager()->getStorage('cml')->load($cml_id);
        if ($cml) {
          $cml->set('state', 'failure')->save();
          \Drupal::logger(__CLASS__)->warning("$cml_id - set state to failure.");
        }
        return TRUE;
      }
      \Drupal::logger(__CLASS__)->notice("Migrations are still running.");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clear Cmlmigrations.
   */
  private function clearCmlmigrations() : bool {
    $cml_migrations_time = \Drupal::state()->get('cml_migrations');
    if ($cml_migrations_time) {
      if (time() - $cml_migrations_time > 3600) {
        \Drupal::logger(__CLASS__)->notice("Set process CmlMigrations free.");
        \Drupal::state()->set('cml_migrations', FALSE);
        return TRUE;
      }
      \Drupal::logger(__CLASS__)->notice("Process CmlMigrations in progress.");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Reset a active migration's status to idle.
   */
  private function resetStatusMigrations(): void {
    $migrations = \Drupal::service('cmlmigrations.migrate')->getCmlGroup();
    if ($migrations['status']) {
      return;
    }
    foreach ($migrations['list'] as $migration_id => $migration_data) {
      if ($migration_data['status'] !== 'Idle') {
        $this->resetStatus($migration_id);
      }
    }
  }

  /**
   * Reset a active migration's status to idle.
   */
  private function resetStatus(string $migration_id = ''): void {
    $migration = $this->migrationPluginManager->createInstance($migration_id);
    if ($migration) {
      // 0 - Idle.
      $migration->setStatus(0);
    }
  }

}
