<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_commerce_product_variation"
 * )
 */
class CommerceProductVariation extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $config = \Drupal::config('cmlmigrations.settings');
    $this->fetch = $config->get('plugin-variation-fetch') ?? FALSE;
    $this->debug = $config->get('plugin-variation-debug') ?? FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $rows = [];
    $type = \Drupal::config('cmlmigrations.settings')->get('variation');
    $stores = \Drupal::config('cmlmigrations.settings')->get('stores') ?? FALSE;
    $prices = \Drupal::config('cmlmigrations.settings')->get('prices') ?? FALSE;
    $source = \Drupal::service('cmlapi.parser_offers')->parseArray();
    $priceTypeDefaultId = '';
    if (!empty($source['price'])) {
      foreach ($source['price'] as $id => $value) {
        if ($value['Наименование'] == 'Розничная цена') {
          $priceTypeDefaultId = $id;
        }
      }
    }
    if (!empty($source['offer'])) {
      $currency_code = 'RUB';
      /** @var \Drupal\commerce_product\Entity\ProductAttributeValue $storage */
      $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_attribute_value');
      $attributes = $this->queryMap('migrate_map_cml_product_variation_attribute');
      $trans = \Drupal::transliteration();
      $k = 0;
      foreach ($source['offer'] as $key => $row) {
        if ($k++ < 100 || !$this->uipage) {
          $id = $row['Id'];
          $status = isset($row['status']) && $row['status'] == 'Удален' ? 0 : 1;
          $price = empty($row['Ceny']) ? 0 : $row['Ceny'][0]['ЦенаЗаЕдиницу'];
          $rows[$id] = [
            'type' => $type,
            'uuid' => $id,
            'sku' => $id,
            'status' => $status,
            'product_uuid' => strstr("{$id}#", "#", TRUE),
            'title' => $this->getVal($row, 'Naimenovanie'),
            'unit' => $this->getVal($row, 'BazovayaEdinica'),
            'field_stock' => $this->getVal($row, 'Kolichestvo'),
            // 'Sklad' => $this->getVal($row, 'Sklad'),
            'price' => [
              'number' => $price,
              'currency_code' => $currency_code,
            ],
          ];
          if (!empty($row['Ceny']) && $prices) {
            $priceTypes = [];
            foreach ($row['Ceny'] as $priceType) {
              $price = [
                'number' => $priceType['ЦенаЗаЕдиницу'],
                'currency_code' => $currency_code,
              ];
              $priceTypes[$priceType['ИдТипаЦены']] = $price;
              if ($priceType['ИдТипаЦены'] == $priceTypeDefaultId) {
                $rows[$id]['price'] = $price;
              }
            }
            $rows[$id]['field_json_prices'] = json_encode($priceTypes);
          }
          if (!empty($row['ZnacheniyaSvoystv'])) {
            foreach ($row['ZnacheniyaSvoystv'] as $value) {
              if (array_key_exists($value, $attributes)) {
                $field = "attribute_{$storage->load($attributes[$value])->getAttributeId()}";
                $rows[$id][$field] = [
                  'target_id' => $attributes[$value],
                ];
              }
            }
          }
          if (!empty($row['Sklad']) && $stores) {
            $sklads = [];
            foreach ($row['Sklad'] as $sklad) {
              $sklads[$sklad['@attributes']['ИдСклада']] = $sklad['@attributes']['КоличествоНаСкладе'];
            }
            $rows[$id]['field_json_stores'] = json_encode($sklads);
          }
        }
      }
    }
    return $rows;
  }

  /**
   * Query MAP.
   */
  private function queryMap($table, $subs = FALSE) {
    $data = [];
    $db = \Drupal::database();
    if (!$db->schema()->tableExists($table)) {
      return [];
    }
    $query = $db->select($table, 'map')->fields('map', [
      'sourceid1',
      'destid1',
    ]);
    $res = $query->execute();
    if ($res) {
      foreach ($res as $key => $row) {
        $data[$row->sourceid1] = $row->destid1;
      }
    }
    return $data;
  }

}
