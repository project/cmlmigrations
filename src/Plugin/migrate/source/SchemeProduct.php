<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_scheme_product"
 * )
 */
class SchemeProduct extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows($rows = []) {
    $k = 0;
    $source = \Drupal::service('cmlmigrations.scheme')->productTypes();
    foreach ($source as $key => $row) {
      if ($k++ < 700 || !$this->uipage) {
        $rows[$key] = [
          'id' => $row['machine'],
          'uuid' => $key,
          'type' => $row['machine'],
          'status' => 1,
          'label' => $row['name'],
          'description' => FALSE,
          'variationType' => 'variation',
          'injectVariationFields' => TRUE,
        ];
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

}
