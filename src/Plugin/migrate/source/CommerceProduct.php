<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\FindImage;
use Drupal\cmlmigrations\Utility\FindVariation;
use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_commerce_product"
 * )
 */
class CommerceProduct extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $config = \Drupal::config('cmlmigrations.settings');
    $this->fetch = $config->get('plugin-product-fetch') ?? FALSE;
    $this->debug = $config->get('plugin-product-debug') ?? FALSE;
    $this->images = [];
    $this->variations = [];
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $rows = [];
    $type = \Drupal::config('cmlmigrations.settings')->get('product');
    $source = \Drupal::service('cmlapi.parser_product')->parse();
    if (!empty($source['data'])) {
      $store = $this->getStore();
      $moysklad = FALSE;
      if ($this->checkProductId(array_key_first($source['data']))) {
        $moysklad = TRUE;
        $this->changeConfig();
      }
      if (count($source['data']) > 400) {
        // Если вариаций много - грузим сразу все.
        $this->variations = FindVariation::getBy1cUuid();
        $this->images = FindImage::getBy1cImage();
      }
      foreach ($source['data'] as $id => $row) {
        if ($k++ < 100 || !$this->uipage) {
          $product = $row['product'];
          $offers = $row['offers'];
          $body = '';
          // Важно для миграции с мой склад, где картинки идут в отдельном файле import.xml.
          if ($moysklad) {
            if (!isset($product['Gruppy'])) {
              continue;
            }
            if (isset($product['ZnacheniyaRekvizitov']['Полное наименование'])) {
              $body = $product['ZnacheniyaRekvizitov']['Полное наименование'];
            }
          }
          else {
            $body = $product['Opisanie'];
          }
          $status = isset($product['status']) && $product['status'] == 'Удален' ? 0 : 1;
          $rows[$id] = [
            'uuid' => $id,
            'type' => $type,
            'stores' => $store,
            'status' => $status,
            'title' => trim($product['Naimenovanie'] ?? ""),
            'catalog' => $product['Gruppy'][0] ?? [],
            'field_article' => $product['Artikul'],
            'body' => [
              'value' => nl2br($body ?? ""),
              'format' => 'basic_html',
            ],
          ];
          $this->hasVariations($rows, "$id");
          $this->hasImage($rows, $product, "$id");
        }
      }
    }
    return $rows;
  }

  /**
   * HasVariations.
   */
  private function hasVariations(array &$rows, string $id) : void {
    $variations = $this->variations;
    if (empty($variations)) {
      // Ищем вариации текущего товара.
      $variations = FindVariation::getBy1cUuid($id, FALSE);
    }
    if (isset($variations[$id])) {
      $result = $variations[$id];
      $rows[$id]['variations'] = $result;
    }
  }

  /**
   * HasImage.
   */
  private function hasImage(array &$rows, array $product, string $id, string $field = 'Kartinka') : void {
    if (!empty($product[$field])) {
      foreach ($product[$field] as $key => $image) {
        if (!empty($this->images)) {
          $result = $this->images[$image] ?? FALSE;
        }
        else {
          $img = FindImage::getBy1cImage($image, FALSE);
          $result = $img[$image] ?? FALSE;
        }
        if ($result) {
          switch ($key) {
            case 0:
              $rows[$id]['field_image'] = [
                'target_id' => $result,
              ];
              break;

            default:
              $rows[$id]['field_gallery'][] = [
                'target_id' => $result,
              ];
              break;
          }
        }
      }
    }
  }

  /**
   * ChangeConfig.
   */
  private function changeConfig() {
    $changes = FALSE;
    $config = \Drupal::configFactory()->getEditable('migrate_plus.migration.cml_product');
    if ($config->get('process.field_image')) {
      $config->clear('process.field_image');
      $changes = TRUE;
    }
    if ($config->get('process.field_gallery')) {
      $config->clear('process.field_gallery');
      $changes = TRUE;
    }
    if ($changes) {
      $config->save();
      drupal_flush_all_caches();
    }
  }

  /**
   * CheckProductId.
   */
  private function checkProductId($pid) {
    if (strlen($pid) !== 36) {
      return TRUE;
    }
    $parts = explode('-', $pid);
    if (count($parts) !== 5) {
      return TRUE;
    }
    if (strlen($parts[0]) !== 8) {
      return TRUE;
    }
    if (strlen($parts[1]) !== 4) {
      return TRUE;
    }
    if (strlen($parts[2]) !== 4) {
      return TRUE;
    }
    if (strlen($parts[3]) !== 4) {
      return TRUE;
    }
    if (strlen($parts[4]) !== 12) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * GetStore.
   */
  private function getStore() : int {
    $sid = 0;
    /** @var \Drupal\commerce_store\StoreStorageInterface $storage */
    $storage = \Drupal::service('entity_type.manager')->getStorage('commerce_store');
    if ($store = $storage->loadDefault()) {
      $sid = $store->id();
    }
    else {
      $ids = $storage->getQuery()
        ->range(0, 1)
        ->accessCheck(TRUE)
        ->execute();
      if (!empty($ids)) {
        $sid = array_shift($ids);
        $store = $storage->load($sid);
        $store->is_default->setValue(TRUE);
        $store->save();
      }
    }
    return $sid;
  }

}
