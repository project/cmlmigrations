<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_tx_catalog"
 * )
 */
class TaxonomyCatalog extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $rows = [];
    $source = \Drupal::service('cmlapi.parser_catalog')->parse();
    if ($source) {
      $k = 0;
      $vocabulary = \Drupal::config('cmlmigrations.settings')->get('vocabulary');
      foreach ($source['catalog'] as $key => $row) {
        if ($k++ < 700 || !$this->uipage) {
          $rows[$key] = [
            'vid' => $vocabulary,
            'uuid' => $row['id'],
            'status' => !$row['delete'] ? 1 : 0,
            'name' => $row['name'],
            'weight' => $row['term_weight'],
          ];
          if (isset($row['parent']) && $row['parent']) {
            $rows[$key]['parent'] = $row['parent'];
          }
        }
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

}
