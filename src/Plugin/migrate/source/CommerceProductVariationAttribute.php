<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\cmlmigrations\Utility\Service;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_commerce_product_variation_attribute"
 * )
 */
class CommerceProductVariationAttribute extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $rows = [];
    $source = \Drupal::service('cmlapi.parser_offers')->parseArray();
    $trans = \Drupal::transliteration();
    if (!empty($source['svoistvo'])) {
      $k = 0;
      $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_attribute');
      foreach ($source['svoistvo'] as $values) {
        if ($values['ТипЗначений'] == 'Справочник') {
          $name = Service::getNormalizeName($trans->transliterate(mb_strtolower($values['Наименование']), ''));
          if (array_key_exists($name, $this->attributes)) {
            $name = $this->attributes[$name];
          }
          $query = $storage
            ->getQuery()
            ->condition('id', $name)
            ->accessCheck(TRUE)
            ->execute();
          if (empty($query)) {
            $attribute = $storage->create([
              'id' => $name,
              'label' => $values['Наименование'],
            ]);
            $attribute->save();
          }
          else {
            $attribute = $storage->load($name);
          }
          $attribute_field_manager = \Drupal::service('commerce_product.attribute_field_manager');
          $type = \Drupal::config('cmlmigrations.settings')->get('variation');
          $fields = $attribute_field_manager->getFieldMap($type);
          $attributes = [];
          foreach ($fields as $value) {
            $attributes[] = $value['attribute_id'];
          }
          if (!in_array($attribute->id(), $attributes)) {
            $attribute_field_manager->createField($attribute, $type);
          }
          if (empty($values['ВариантыЗначений']['Справочник'])) {
            continue;
          }
          elseif ($this->arrayIsList($values['ВариантыЗначений']['Справочник'])) {
            $dictionary = $values['ВариантыЗначений']['Справочник'];
          }
          else {
            $dictionary = [$values['ВариантыЗначений']['Справочник']];
          }
          foreach ($dictionary as $value) {
            $id = $value['ИдЗначения'];
            $rows[$id] = [
              'uuid' => $id,
              'attribute' => $name,
              'name' => $value['Значение'],
              'weight' => 0,
            ];
          }
        }
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

  /**
   * Get Normalize Name.
   */
  public function getNormalizeName(string $name): string {
    $name = trim($name);
    $name = str_replace([',', '.', '(', ')', '"', "'"], '', $name);
    $name = str_replace([' ', '-'], '_', $name);
    // Ограничение на кол-во символов в машинном имени поля.
    $name = substr($name, 0, 22);
    $name = trim($name, '_');
    return $name;
  }

  /**
   * {@inheritdoc}
   */
  private function arrayIsList(array $array) : bool {
    if (function_exists('array_is_list')) {
      return array_is_list($array);
    }
    if ($array === []) {
      return TRUE;
    }
    return array_keys($array) === range(0, count($array) - 1);
  }

}
