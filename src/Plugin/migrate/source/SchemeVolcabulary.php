<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_scheme_volcabulary"
 * )
 */
class SchemeVolcabulary extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows($rows = []) {
    $k = 0;
    $source = \Drupal::service('cmlmigrations.scheme')->vocabularies();
    foreach ($source as $key => $row) {
      if ($k++ < 700 || !$this->uipage) {
        $rows[$key] = [
          'uuid' => $key,
          'vid' => $row['machine'],
          'name' => $row['name'],
          'description' => FALSE,
          'weight' => 0,
        ];
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'vid' => $this->t('The vocabulary ID.'),
      'name' => $this->t('The name of the vocabulary.'),
      'description' => $this->t('The description of the vocabulary.'),
      'hierarchy' => $this->t('The type of hierarchy allowed within the vocabulary. (0 = disabled, 1 = single, 2 = multiple)'),
      'module' => $this->t('Module responsible for the vocabulary.'),
      'weight' => $this->t('The weight of the vocabulary in relation to other vocabularies.'),
      'machine_name' => $this->t('Unique machine name of the vocabulary.'),
    ];
  }

}
