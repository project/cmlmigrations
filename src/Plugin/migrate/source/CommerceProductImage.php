<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\FindImage;
use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\file\Entity\File;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_commerce_product_image"
 * )
 */
class CommerceProductImage extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $rows = [];
    $source = FALSE;
    $images = FALSE;
    $variations = FALSE;
    $source = \Drupal::service('cmlapi.parser_product')->parse();
    if (!empty($source['data'])) {
      $store = $this->getStore();
      if (count($source['data']) > 500) {
        // Если вариаций много - грузим сразу все.
        $images = FindImage::getBy1cImage();
      }
      $storage = \Drupal::entityTypeManager()->getStorage('commerce_product');
      foreach ($source['data'] as $id => $row) {
        if ($k++ < 100 || !$this->uipage) {
          $product = $row['product'];
          if (!isset($product['Gruppy']) && isset($product['Kartinka'])) {
            $offers = $row['offers'];
            $rows[$id] = [
              'uuid' => $id,
            ];
            $this->updateMapping($id);
            $this->hasImage($rows, $images, $product, $id);
          }
        }
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

  /**
   * Update mapping.
   */
  private function updateMapping($sourceId) {
    $querySource = \Drupal::database()->select('migrate_map_cml_product', 'maps_source');
    $querySource->fields('maps_source', [
      'source_ids_hash',
      'sourceid1',
      'destid1',
    ])
      ->condition('sourceid1', $sourceId);
    $resSource = $querySource->execute();

    if ($resSource && \Drupal::database()->schema()->tableExists('migrate_map_cml_product_image')) {
      foreach ($resSource as $rowSource) {
        $upsert = \Drupal::database()->upsert('migrate_map_cml_product_image')
          ->fields([
            'source_ids_hash' => $rowSource->source_ids_hash,
            'sourceid1' => $rowSource->sourceid1,
            'destid1' => $rowSource->destid1,
          ])
          ->key('sourceid1')
          ->execute();
      }
    }
  }

  /**
   * HasImage.
   */
  private function hasImage(&$rows, $images, $product, $id, $field = 'Kartinka') {
    $gallery = FALSE;
    if (!empty($product[$field])) {
      foreach ($product[$field] as $key => $image) {
        if (!$images) {
          $result = FindImage::getBy1cImage($image, FALSE);
          if (empty($result)) {
            $result = $this->restoreImage($image);
          }
          else {
            $result = $result[$image];
          }
        }
        if (isset($images[$image])) {
          $result = $images[$image];
        }
        switch ($key) {
          case 0:
            $rows[$id]['field_image'] = [
              'target_id' => $result,
            ];
            break;

          default:
            $rows[$id]['field_gallery'][] = [
              'target_id' => $result,
            ];
            break;
        }
      }
    }
  }

  /**
   * Restore images in file_managed.
   */
  private function restoreImage($image) {
    $config = \Drupal::config('cmlexchange.settings');
    $dir = 'cml-files';
    if ($config->get('file-path')) {
      $dir = $config->get('file-path');
    }
    $file = "public://$dir/$image";
    $file = File::create([
      'uid' => 1,
      'filename' => $image,
      'uri' => $file,
      'filemime' => 'image/' . pathinfo($file, PATHINFO_EXTENSION),
      'status' => 1,
    ]);
    $file->setPermanent();
    $file->save();
    return $file->id();
  }

  /**
   * GetStore.
   */
  private function getStore() {
    $sid = FALSE;
    /** @var \Drupal\commerce_store\StoreStorageInterface $storage */
    $storage = \Drupal::service('entity_type.manager')->getStorage('commerce_store');
    if ($store = $storage->loadDefault()) {
      $sid = $store->id();
    }
    else {
      $ids = $storage->getQuery()
        ->range(0, 1)
        ->accessCheck(TRUE)
        ->execute();
      if (!empty($ids)) {
        $sid = array_shift($ids);
        $store = $storage->load($sid);
        $storage->markAsDefault($store);
      }
    }
    return $sid;
  }

}
