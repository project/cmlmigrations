<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for XML.
 *
 * @MigrateSource(
 *   id = "cml_commerce_product_variation_rest"
 * )
 */
class CommerceProductVariationRest extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $rows = [];
    $source = \Drupal::service('cmlapi.parser_rests')->parse();
    if ($source) {
      $k = 0;
      foreach ($source as $key => $row) {
        if ($k++ < 50 || !$this->uipage) {
          $id = $row['Id'];
          $rows[$id] = [
            'uuid' => $id,
          ];
          $variationId = $this->updateMapping($id);
          $sklads = [];
          foreach ($row['Ostatki'][0]['Остаток'] as $value) {
            $sklads[$value['Склад']['Ид']] = $value['Склад']['Количество'];
          }
          $rows[$id]['field_json_rest'] = json_encode($sklads);
        }
      }
    }
    $this->debug = TRUE;
    return $rows;
  }

  /**
   * Update mapping.
   */
  private function updateMapping($sourceId) {
    $destid = FALSE;
    $querySource = \Drupal::database()->select('migrate_map_cml_product_variation', 'maps_source');
    $querySource->fields('maps_source', [
      'source_ids_hash',
      'sourceid1',
      'destid1',
    ])
      ->condition('sourceid1', $sourceId);
    $resSource = $querySource->execute();

    if ($resSource && \Drupal::database()->schema()->tableExists('migrate_map_cml_product_variation_rest')) {
      foreach ($resSource as $rowSource) {
        $destid = $rowSource->destid1;
        $upsert = \Drupal::database()->upsert('migrate_map_cml_product_variation_rest')
          ->fields([
            'source_ids_hash' => $rowSource->source_ids_hash,
            'sourceid1' => $rowSource->sourceid1,
            'destid1' => $destid,
          ])
          ->key('sourceid1')
          ->execute();
      }
    }
    return $destid;
  }

}
