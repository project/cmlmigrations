<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_tx_terms"
 * )
 */
class TaxonomyTerms extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows($rows = []) {
    $k = 0;
    $source = \Drupal::service('cmlmigrations.scheme')->terms();
    foreach ($source as $key => $row) {
      if ($k++ < 700 || !$this->uipage) {
        $rows[$key] = [
          'uuid' => $key,
          'vid' => $row['vid'],
          'name' => $row['name'],
          'status' => 1,
          'weight' => 0,
        ];
        if (isset($row['term_weight']) && $row['term_weight']) {
          $rows[$key]['term_weight'] = $row['term_weight'];
        }
        if (isset($row['parent']) && $row['parent']) {
          $rows[$key]['parent'] = $row['parent'];
        }
      }
    }
    $this->debug = TRUE;
    return $rows;
  }

}
