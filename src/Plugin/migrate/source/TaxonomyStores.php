<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_tx_stores"
 * )
 */
class TaxonomyStores extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $stores = \Drupal::config('cmlmigrations.settings')->get('stores') ?? FALSE;
    if (!$stores) {
      return [];
    }
    $rows = [];
    $source = \Drupal::service('cmlapi.parser_offers')->parseArray();
    if (!empty($source['stock'])) {
      $storageTaxonomyVocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
      $vid = 'stores';
      if (is_null($storageTaxonomyVocabulary->load($vid))) {
        $vocabulary = $storageTaxonomyVocabulary->create([
          'name' => 'Stores',
          'vid' => $vid,
        ])->save();
      }
      $k = 0;
      foreach ($source['stock'] as $key => $row) {
        if ($k++ < 700 || !$this->uipage) {
          $rows[$key] = [
            'vid' => $vid,
            'uuid' => $row['Ид'],
            'status' => 1,
            'name' => $row['Наименование'],
            'weight' => 0,
          ];
        }
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

}
