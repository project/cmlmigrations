<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for XML.
 *
 * @MigrateSource(
 *   id = "cml_commerce_product_variation_price"
 * )
 */
class CommerceProductVariationPrice extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $rows = [];
    $source = \Drupal::service('cmlapi.parser_prices')->parse();
    if ($source) {
      $k = 0;
      foreach ($source as $key => $row) {
        if ($k++ < 50 || !$this->uipage) {
          $id = $row['Id'];
          $rows[$id] = [
            'uuid' => $id,
          ];
          $variationId = $this->updateMapping($id);
          if ($variationId) {
            $rows[$id]['price'] = [
              'number' => (int) $row['Ceny'][0]['ЦенаЗаЕдиницу'],
              'currency_code' => 'RUB',
            ];
            if (!strpos($id, '#')) {
              $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_variation');
              $ids = $storage
                ->getQuery()
                ->condition('status', 1)
                ->condition('sku', "$id#%", 'LIKE')
                ->accessCheck(TRUE)
                ->execute();
              if ($ids) {
                foreach ($storage->loadMultiple($ids) as $variation) {
                  $variationId = $this->updateMapping($variation->sku->value);
                  if ($variationId) {
                    $rows[$variation->sku->value] = [
                      'uuid' => $variation->sku->value,
                      'price' => [
                        'number' => (int) $row['Ceny'][0]['ЦенаЗаЕдиницу'],
                        'currency_code' => 'RUB',
                      ],
                    ];
                  }
                }
              }
            }
          }
        }
      }
    }
    $this->debug = TRUE;
    return $rows;
  }

  /**
   * Update mapping.
   */
  private function updateMapping($sourceId) {
    $destid = FALSE;
    $querySource = \Drupal::database()->select('migrate_map_cml_product_variation', 'maps_source');
    $querySource->fields('maps_source', [
      'source_ids_hash',
      'sourceid1',
      'destid1',
    ])
      ->condition('sourceid1', $sourceId);
    $resSource = $querySource->execute();

    if ($resSource && \Drupal::database()->schema()->tableExists('migrate_map_cml_product_variation_price')) {
      foreach ($resSource as $rowSource) {
        $destid = $rowSource->destid1;
        $upsert = \Drupal::database()->upsert('migrate_map_cml_product_variation_price')
          ->fields([
            'source_ids_hash' => $rowSource->source_ids_hash,
            'sourceid1' => $rowSource->sourceid1,
            'destid1' => $rowSource->destid1,
          ])
          ->key('sourceid1')
          ->execute();
      }
    }
    return $destid;
  }

}
