<?php

namespace Drupal\cmlmigrations\Plugin\migrate\source;

use Drupal\cmlmigrations\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "cml_tx_prices"
 * )
 */
class TaxonomyPrices extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = FALSE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $prices = \Drupal::config('cmlmigrations.settings')->get('prices') ?? FALSE;
    if (!$prices) {
      return [];
    }
    $rows = [];
    $source = \Drupal::service('cmlapi.parser_offers')->parseArray();
    if (!empty($source['price'])) {
      $storageTaxonomyVocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
      $vid = 'prices';
      if (is_null($storageTaxonomyVocabulary->load($vid))) {
        $vocabulary = $storageTaxonomyVocabulary->create([
          'name' => 'Price Types',
          'vid' => $vid,
        ])->save();
      }
      $k = 0;
      foreach ($source['price'] as $key => $row) {
        if ($k++ < 700 || !$this->uipage) {
          $rows[$key] = [
            'vid' => $vid,
            'uuid' => $row['Ид'],
            'status' => 1,
            'name' => $row['Наименование'],
            'weight' => 0,
          ];
        }
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

}
