<?php

namespace Drupal\cmlmigrations\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin iterates and processes an array.
 *
 * @link https://www.drupal.org/node/2135345 Online handbook documentation for iterator process plugin @endlink
 *
 * @MigrateProcessPlugin(
 *   id = "multi_target",
 *   handle_multiples = TRUE
 * )
 */
class MultiTarget extends ProcessPluginBase {

  /**
   * Runs a process pipeline on each destination property per list item.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $return = [];
    // Target = target_id.
    $src = $this->configuration['target'];
    if (!empty($value[0])) {
      foreach ($value as $k => $v) {
        if (!empty($v[$src])) {
          $return[$k] = $v;
        }
        elseif (is_numeric($v)) {
          $return[$k] = ['target_id' => (int) $v];
        }
        else {
          \Drupal::logger('cmlmigrations/MultiTarget')->notice("Bad MultiTarget value:\n", [
            '@hello' => print_r($v, TRUE),
          ]);
        }
      }
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

}
