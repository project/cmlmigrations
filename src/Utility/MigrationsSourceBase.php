<?php

namespace Drupal\cmlmigrations\Utility;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for Plugins.
 */
class MigrationsSourceBase extends SourcePluginBase {

  //phpcs:disable
  protected string $plugin_id;
  protected array $rows;
  protected array $attributes;
  protected array $config;
  protected bool $fetch;
  protected bool $debug;
  protected bool $uipage;
  protected bool $statuspage;
  protected array $images;
  protected array $variations;
  //phpcs:enable

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    // SourcePlugin Settings.
    $this->plugin_id = $plugin_id;
    $this->trackChanges = TRUE;
    $this->setupDefaultProps();

    $this->config = self::accessProtected($migration, 'pluginDefinition')['process'];
    // Get Rows on Construct.
    if ($this->fetch) {
      $rows = $this->getRows();
      $this->rows = $rows;
    }
    // Debug.
    if ($this->uipage && $this->debug && \Drupal::moduleHandler()->moduleExists('devel')) {
      dsm("$plugin_id: ProcessMapping & Rows");
      dsm($this->config);
      dsm($rows ?? 'Rows not feched');
    }
  }

  /**
   * Default Props.
   */
  private function setupDefaultProps() {
    $this->rows = [];
    $this->attributes = [
      'razmer' => 'size',
      'cvet' => 'color',
    ];
    // Fetch.
    if (!property_exists($this, 'fetch')) {
      $this->fetch = FALSE;
    }
    // Debug.
    if (!property_exists($this, 'debug')) {
      $this->debug = FALSE;
    }
    // UiPage.
    $this->uipage = FALSE;
    if (\Drupal::routeMatch()->getRouteName() == "entity.migration.list") {
      $this->uipage = TRUE;
    }
  }

  /**
   * Get Rows.
   */
  public function getRows() {
    $rows = [];
    $this->rows = $rows;
    return $rows;
  }

  /**
   * Check .
   */
  public function getVal($row, $val) {
    $result = FALSE;
    if (isset($row[$val])) {
      $result = $row[$val];
    }
    return $result;
  }

  /**
   * UiPage.
   */
  public function uiPage() {
    $uipage = FALSE;
    $statuspage = FALSE;
    if (\Drupal::routeMatch()->getRouteName() == "entity.migration.list") {
      $uipage = TRUE;
    }
    if (\Drupal::routeMatch()->getRouteName() == "cmlmigrations.status") {
      $statuspage = TRUE;
    }
    $this->uipage = $uipage;
    $this->statuspage = $statuspage;
    return $uipage;
  }

  /**
   * Access Protected Obj Property.
   */
  public static function accessProtected($obj, $prop) {
    $reflection = new \ReflectionClass($obj);
    $property = $reflection->getProperty($prop);
    $property->setAccessible(TRUE);
    return $property->getValue($obj);
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    $rows = $this->getRows();
    return new \ArrayIterator($rows);
  }

  /**
   * Allows class to decide how it will react when it is treated like a string.
   */
  public function __toString() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getIDs() {
    return [
      'uuid' => [
        'type' => 'string',
        'alias' => 'id',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'uuid' => $this->t('1C UUID Key'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return count($this->rows);
  }

}
