<?php

namespace Drupal\cmlmigrations\Utility;

/**
 * Find Image.
 */
class FindImage {

  /**
   * Find images.
   */
  public static function getBy1cImage(string $img = 'import_files||imageid', bool $all = TRUE) : array {
    $images = [];
    $image = explode('||', $img);

    if ($image || $all) {
      $query = \Drupal::database()->select('file_managed', 'files')
        ->fields('files', [
          'fid',
          'uri',
        ]);
      $orCondition = $query->orConditionGroup();
      foreach ($image as $value) {
        $orCondition->condition('uri', "%$value%", 'LIKE');
      }
      $query->condition($orCondition);
      $res = $query->execute();
      if ($res) {
        $config = \Drupal::config('cmlexchange.settings');
        $dir = 'cml-files';
        if ($config->get('file-path')) {
          $dir = $config->get('file-path');
        }
        $filepath = "public://{$dir}/";
        foreach ($res as $file) {
          $uri = substr($file->uri, strlen($filepath));
          $fid = $file->fid;
          $images[$uri] = $fid;
        }
      }
    }
    return $images;
  }

}
