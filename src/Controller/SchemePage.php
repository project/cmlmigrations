<?php

namespace Drupal\cmlmigrations\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for scheme page.
 */
class SchemePage extends ControllerBase {

  /**
   * Page.
   */
  public function page() {
    $cml_service = \Drupal::service('cmlapi.cml');
    if ($cml = $cml_service->actual()) {
      $cid = $cml->id();
    }
    $scheme = \Drupal::service('cmlmigrations.scheme')->vocabularies($cid);
    $output = "hello world";
    if (FALSE) {
      // $form = \Drupal::formBuilder()->getForm($exec, $migrations['list']);
    }

    return [
      'output' => [
        '#markup' => $output,
      ],
    ];
  }

}
