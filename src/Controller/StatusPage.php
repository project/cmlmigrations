<?php

namespace Drupal\cmlmigrations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Controller routines for status page.
 */
class StatusPage extends ControllerBase {

  /**
   * Page.
   */
  public function page() {

    $output = "<h3>Актуальный обмен</h3>";
    $time = \Drupal::time()->getRequestTime();
    $now = DrupalDateTime::createFromTimestamp($time);
    $cml_service = \Drupal::service('cmlapi.cml');
    if ($cml = $cml_service->actual()) {
      $created = DrupalDateTime::createFromTimestamp($cml->created->value);
      $changed = DrupalDateTime::createFromTimestamp($cml->changed->value);
      $id = $cml->id();
      $output .= "&mdash; [{$id}] - ";
      $output .= $cml->getState();
      $output .= " / Ago: ";
      $output .= $changed->diff($now)->format('%H:%i:%s');
      $output .= " / Time: ";
      $output .= $changed->diff($created)->format('%H:%i:%s');
      $output .= "<br>";
    }
    else {
      $output .= "&mdash; <br>";
    }
    $output .= "<h3>Текущий обмен</h3>";
    if ($cml = $cml_service->current()) {
      $created = DrupalDateTime::createFromTimestamp($cml->created->value);
      $changed = DrupalDateTime::createFromTimestamp($cml->changed->value);
      $id = $cml->id();
      $output .= "&mdash; [{$id}] - ";
      $output .= $cml->getState();
      $output .= " / Ago: ";
      $output .= $changed->diff($now)->format('%H:%i:%s');
      $output .= " / Time: ";
      $output .= $changed->diff($created)->format('%H:%i:%s');
      $output .= "<br>";
    }
    else {
      $output .= "&mdash; <br>";
    }
    $output .= "<h3>Следующий обмен</h3>";
    if ($cml = $cml_service->next()) {
      $created = DrupalDateTime::createFromTimestamp($cml->created->value);
      $changed = DrupalDateTime::createFromTimestamp($cml->changed->value);
      $id = $cml->id();
      $output .= "&mdash; [{$id}] - ";
      $output .= $cml->getState();
      $output .= "<br>";
    }
    else {
      $output .= "&mdash; <br>";
    }
    $output .= "<h3>Очередь обменов</h3>";
    if (!empty($cmllist = $cml_service->all())) {
      $time = \Drupal::time()->getRequestTime();
      foreach ($cmllist as $id => $cml) {
        $diff = $time - $cml->created->value;
        $output .= "&mdash; [{$id}] - ";
        $output .= $cml->getState();
        $output .= " / Date: ";
        $output .= \Drupal::service('date.formatter')->format($diff, 'custom', 'H:i:s');
        $output .= "<br>";
      }
    }
    else {
      $output .= "&mdash; <br>";
    }
    $migrations = \Drupal::service('cmlmigrations.migrate')->getCmlGroup();
    $output .= "<h3>Статус</h3>";
    if ($migrations['list']) {
      $output .= "Готов<br>";
    }
    else {
      $output .= "Занят<br>";
    }
    $exec = 'Drupal\cmlmigrations\Form\ExecMigrations';
    $form = \Drupal::formBuilder()->getForm($exec, $migrations['list']);

    return [
      'output' => ['#markup' => $output],
      'form' => $form,
      'migr-table' => $this->statusTable($migrations),
    ];
  }

  /**
   * Status Table.
   */
  private function statusTable(array $migrations) : array {

    $rows = [];
    if ($migrations) {
      foreach ($migrations['list'] as $id => $migration) {
        $date_formatter = \Drupal::service('date.formatter');
        $last = 'N/A';
        if (is_numeric($migration['last'])) {
          $last_time = round($migration['last'] / 1000);
          $last = $date_formatter->format($last_time, 'custom', 'dM H:i:s');
        }
        $rows[] = [
          'label' => $migration['label'],
          'status' => $migration['status'],
          'total' => $migration['total'],
          'imported' => $migration['imported'],
          'unprocessed' => $migration['unprocessed'],
          'messages' => $migration['messages'],
          'last' => $last,
        ];
      }
    }
    return [
      '#type' => 'table',
      '#header' => $this->statusTableHeader(),
      '#rows' => $rows,
    ];
  }

  /**
   * Status Table Header.
   */
  private function statusTableHeader() {
    $header = [
      'label' => $this->t('Migration'),
      'status' => $this->t('Status'),
      'total' => $this->t('Total'),
      'imported' => $this->t('Imported'),
      'unprocessed' => $this->t('Unprocessed'),
      'messages' => $this->t('Messages'),
      'last' => $this->t('Last'),
    ];
    return $header;
  }

}
