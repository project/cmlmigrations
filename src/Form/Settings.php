<?php

namespace Drupal\cmlmigrations\Form;

use Drupal\cmlmigrations\Utility\Service;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Implements the form controller.
 */
class Settings extends ConfigFormBase {

  // phpcs:disable
  protected ModuleHandlerInterface $moduleHandler;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected EntityTypeBundleInfo $entityTypeBundle;
  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * SettingsFormWarning constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundle
   *   Entity Manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $moduleHandler,
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfo $entityTypeBundle
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundle = $entityTypeBundle;
    parent::__construct($config_factory);
  }

  /**
   * AJAX Responce.
   */
  public static function ajax($otvet) {
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#exec-results", "<pre>{$otvet}</pre>"));
    return $response;
  }

  /**
   * AJAX ProdUuidFill.
   */
  public static function ajaxProdUuidFill(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxProdUuidFill\n";
    $otvet .= Service::uuid1cFill();
    return self::ajax($otvet);
  }

  /**
   * AJAX ProdUuidRemove.
   */
  public static function ajaxProdUuidClear(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxProdUuidClear\n";
    $otvet .= Service::uuid1cClear();
    return self::ajax($otvet);
  }

  /**
   * AJAX Responce.
   */
  public static function ajaxCheckStoresReady(array $form, FormStateInterface &$form_state) {
    $response = new AjaxResponse();
    $storageTaxonomyVocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
    $vid = 'stores';
    if (is_null($storageTaxonomyVocabulary->load($vid))) {
      $vocabulary = $storageTaxonomyVocabulary->create([
        'name' => 'Stores',
        'vid' => $vid,
      ])->save();
    }
    $path = \Drupal::service('extension.list.module')->getPath('cmlmigrations');
    $config_path = "$path/assets/config/stores";
    $source = new FileStorage($config_path);
    \Drupal::service('config.installer')->installOptionalConfig($source);
    foreach (scandir($config_path) as $file) {
      if (substr($file, -4) != '.yml') {
        continue;
      }
      $cfg = substr($file, 0, -4);
      if ($cfg) {
        $data = $source->read($cfg);
        $config = \Drupal::service('config.factory')->getEditable($cfg);
        $config->setData($data)->save();
      }
    }
    $module_handler = \Drupal::moduleHandler();
    $module_handler->invokeAll('cache_flush');
    foreach (Cache::getBins() as $service_id => $cache_backend) {
      $cache_backend
        ->deleteAll();
    }
    $result = 'Сайт готов к миграции магазинов (складов)';
    $response->addCommand(new HtmlCommand("#stores-results", "<pre>$result</pre>"));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cmlmigrations_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cmlmigrations.settings',
      'migrate_plus.migration.cml_taxonomy_catalog',
      'migrate_plus.migration.cml_product_variation',
      'migrate_plus.migration.cml_product',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cmlmigrations.settings');
    $catalog = $this->config('migrate_plus.migration.cml_taxonomy_catalog');
    $product = $this->config('migrate_plus.migration.cml_product');
    $variation = $this->config('migrate_plus.migration.cml_product_variation');
    $source_plugins = $this->getMigrationPlugins();
    $form['mapping'] = [
      '#type' => 'details',
      '#title' => $this->t('Mapping'),
      '#open' => TRUE,
    ];
    if ($catalog->get('process')) {
      $form['migrations-catalog'] = [
        '#type' => 'details',
        '#title' => $this->t('Migrations Catalog'),
        '#open' => TRUE,
        'catalog-source' => [
          '#type' => 'select',
          '#title' => 'Catalog Source Plugin',
          '#options' => $source_plugins,
          '#default_value' => $catalog->get('source')['plugin'],
        ],
        'catalog-process' => [
          '#title' => 'process',
          '#type' => 'textarea',
          '#attributes' => ['data-yaml-editor' => 'true'],
          '#default_value' => Yaml::dump($catalog->get('process'), 4),
        ],
      ];
    }
    if ($variation->get('process')) {
      $form['migrations-variations'] = [
        '#type' => 'details',
        '#title' => $this->t('Migrations Product Variations'),
        '#open' => TRUE,
        'plugin-variation-fetch' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Plugin variation: fetch on construct'),
          '#default_value' => $config->get('plugin-variation-fetch'),
        ],
        'plugin-variation-debug' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Plugin variation: debug on migration list'),
          '#default_value' => $config->get('plugin-variation-debug'),
        ],
        'variations-source' => [
          '#type' => 'select',
          '#title' => 'Product Variations Source Plugin',
          '#options' => $source_plugins,
          '#default_value' => $variation->get('source')['plugin'],
        ],
        'variations-process' => [
          '#title' => 'process',
          '#type' => 'textarea',
          '#attributes' => ['data-yaml-editor' => 'true'],
          '#default_value' => Yaml::dump($variation->get('process'), 4),
        ],
        'variations-attributes' => [
          '#title' => $this->t('Use attributes'),
          '#type' => 'details',
          '#open' => TRUE,
        ],
        'variations-stores' => [
          '#title' => $this->t('Use stores'),
          '#type' => 'details',
          '#open' => TRUE,
        ],
        'variations-prices' => [
          '#title' => $this->t('Use prices'),
          '#type' => 'details',
          '#open' => TRUE,
        ],
      ];
      if ($this->checkStores() > 1) {
        $form['migrations-variations']['variations-stores']['stores-container'] = [
          'stores' => [
            '#type' => 'checkbox',
            '#title' => "Использовать склады с остатками",
            '#default_value' => $config->get('stores'),
          ],
          'check-stores' => [
            '#type' => 'button',
            '#value' => $this->t('Create the required settings'),
            '#ajax'   => [
              'callback' => '::ajaxCheckStoresReady',
              'effect'   => 'fade',
              'progress' => ['type' => 'throbber', 'message' => "..."],
            ],
            '#states' => [
              'disabled' => [
                ":input[data-drupal-selector='edit-stores']" => [
                  'checked' => FALSE,
                ],
              ],
            ],

            '#suffix' => '<div id="stores-results"></div>',
          ],
        ];
      }
      $priceTypes = $this->checkPrices();
      if (count($priceTypes) > 1) {
        $form['migrations-variations']['variations-prices']['prices-container'] = [
          'prices' => [
            '#type' => 'checkbox',
            '#title' => "Использовать типы цен",
            '#default_value' => $config->get('prices'),
          ],
          // phpcs:disable
          // 'price-types' => [
          //   '#type' => 'tableselect',
          //   '#header' => $$priceTypes['header'],
          //   '#options' => $$priceTypes['options'],
          //   '#value' => $this->t('Create the required settings'),
          //   '#ajax'   => [
          //     'callback' => '::ajaxCheckStoresReady',
          //     'effect'   => 'fade',
          //     'progress' => ['type' => 'throbber', 'message' => "..."],
          //   ],
          //   '#states' => [
          //     'disabled' => [
          //       ":input[data-drupal-selector='edit-stores']" => [
          //         'checked' => FALSE,
          //       ],
          //     ],
          //   ],
          //
          //   '#suffix' => '<div id="stores-results"></div>',
          // ],
          // phpcs:enable
        ];
      }
      if ($attributes = $this->checkAttributes()) {
        foreach ($attributes as $key => $value) {
          $form['migrations-variations']['variations-attributes']["checkbox-$key"] = [
            "attribute_{$value['name']}" => [
              '#type' => 'checkbox',
              '#title' => "Использовать $key",
              '#attributes' => [
                'checked' => $value['created'],
                'disabled' => !$value['enable'],
              ],
              // phpcs:disable
              // '#submit' => ['Drupal\cmlmigrations\Form\Settings::attributeChange'],
              // '#ajax'   => [
              //   'callback' => '::ajaxAttributeChange',
              //   'effect'   => 'fade',
              //   'progress' => ['type' => 'throbber', 'message' => "Обновляем..."],
              // ],
              // phpcs:enable
            ],
          ];
        }
      }
    }
    if ($product->get('process')) {
      $form['migrations-product'] = [
        '#type' => 'details',
        '#title' => $this->t('Migrations Product'),
        '#open' => TRUE,
        'plugin-product-fetch' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Plugin product: fetch on construct'),
          '#default_value' => $config->get('plugin-product-fetch'),
        ],
        'plugin-product-debug' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Plugin product: debug on migration list'),
          '#default_value' => $config->get('plugin-product-debug'),
        ],
        'product-source' => [
          '#type' => 'select',
          '#title' => 'Product Source Plugin',
          '#options' => $source_plugins,
          '#default_value' => $product->get('source')['plugin'],
        ],
        'product-process' => [
          '#title' => 'process',
          '#type' => 'textarea',
          '#attributes' => ['data-yaml-editor' => 'true'],
          '#default_value' => Yaml::dump($product->get('process'), 4),
        ],
      ];
    }
    $form['pipeline']['timeout'] = [
      '#title' => $this->t('Timeout'),
      '#type' => 'textfield',
      '#default_value' => $config->get('timeout'),
      '#description' => $this->t('Max migration time before failure (minute)'),
    ];

    $form['mapping']['vocabulary'] = [
      '#title' => $this->t('Catalog Vocabulary'),
      '#type' => 'select',
      '#options' => $this->getTaxonomyBundles(),
      '#default_value' => $config->get('vocabulary'),
    ];
    $form['mapping']['product'] = [
      '#title' => $this->t('Product Destination'),
      '#type' => 'select',
      '#options' => $this->getProductBundles(),
      '#default_value' => $config->get('product'),
    ];
    $form['mapping']['variation'] = [
      '#title' => $this->t('Product Variation Destination'),
      '#type' => 'select',
      '#options' => $this->getProductVariationBundles(),
      '#default_value' => $config->get('variation'),
    ];
    $form['pipeline'] = [
      '#type' => 'details',
      '#title' => $this->t('Pipeline'),
    ];
    $form['pipeline']['timeout'] = [
      '#title' => $this->t('Timeout, minutes'),
      '#type' => 'textfield',
      '#default_value' => $config->get('timeout'),
      '#description' => $this->t('Max migration time before failure (minute)'),
    ];
    $form['pipeline']['timeout-quick-run'] = [
      '#title' => $this->t('Quick Run Timeout, seconds'),
      '#type' => 'textfield',
      '#default_value' => $config->get('timeout-quick-run'),
      '#description' => $this->t('Protect from `migration busy` fails'),
    ];
    $drush_descr = $this->t("Leave blank to use default /var/www/html/vendor/bin/drush");
    $form['drush_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Drush'),
      'drush' => [
        '#title' => $this->t('Drush'),
        '#type' => 'textfield',
        '#default_value' => $config->get('drush'),
        '#description' => $drush_descr,
      ],
    ];
    $form['dev'] = [
      '#type' => 'details',
      '#title' => $this->t('Dev'),
      '#open' => FALSE,
      'bttns' => [
        '#type' => 'actions',
        'fill' => [
          '#type' => 'submit',
          '#value' => 'Fill 1000 product_uuid field',
          '#ajax'   => [
            'callback' => '::ajaxProdUuidFill',
            'effect'   => 'fade',
            'progress' => ['type' => 'throbber', 'message' => ""],
          ],
        ],
        'remove' => [
          '#type' => 'submit',
          '#value' => 'Clear 1000 product_uuid field',
          '#ajax'   => [
            'callback' => '::ajaxProdUuidClear',
            'effect'   => 'fade',
            'progress' => ['type' => 'throbber', 'message' => ""],
          ],
        ],
        '#suffix' => '<div id="exec-results"></div>',
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Product Bundles.
   */
  private function checkAttributes() {
    $cmlService = \Drupal::service('cmlapi.cml');
    $cmls = $cmlService->query(10, ['new', 'progress', 'success'], 'DESC');
    $cid = 0;
    $options = [];
    foreach ($cmls as $key => $cml) {
      $file = $cml->field_file->entity;
      if ($file->getFilename() == 'offers.xml') {
        $cid = $cml->id();
        break;
      }
    }
    if ($cid) {
      $data = \Drupal::service('cmlapi.parser_offers')->parseArray();
      if (!empty($data['svoistvo'])) {
        $trans = \Drupal::transliteration();
        $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_attribute');
        $attributes = [
          'razmer' => 'size',
          'cvet' => 'color',
        ];
        foreach ($data['svoistvo'] as $key => $svoistvo) {
          $name = Service::getNormalizeName($trans->transliterate(mb_strtolower($svoistvo['Наименование']), ''));
          if (array_key_exists($name, $attributes)) {
            $name = $attributes[$name];
          }
          $query = $storage
            ->getQuery()
            ->condition('id', $name)
            ->accessCheck(TRUE)
            ->execute();
          $options[$svoistvo['Наименование']] = [
            'created' => $query ? TRUE : FALSE,
            'enable' => $svoistvo['ТипЗначений'] == 'Справочник' ? TRUE : FALSE,
            'name' => $name,
          ];
        }
      }
    }
    return $options;
  }

  /**
   * Product Bundles.
   */
  private function checkStores() {
    $cmlService = \Drupal::service('cmlapi.cml');
    $cmls = $cmlService->query(10, ['new', 'progress', 'success'], 'DESC');
    $cid = 0;
    $count = 0;
    foreach ($cmls as $key => $cml) {
      $file = $cml->field_file->entity;
      if ($file->getFilename() == 'offers.xml') {
        $cid = $cml->id();
        break;
      }
    }
    if ($cid) {
      $data = \Drupal::service('cmlapi.parser_offers')->parseArray();
      $count = count($data['stock'] ?? []);
    }
    return $count;
  }

  /**
   * Product Bundles.
   */
  private function checkPrices() {
    $cmlService = \Drupal::service('cmlapi.cml');
    $cmls = $cmlService->query(10, ['new', 'progress', 'success'], 'DESC');
    $cid = 0;
    $count = 0;
    $prices = [];
    foreach ($cmls as $key => $cml) {
      $file = $cml->field_file->entity;
      if ($file->getFilename() == 'offers.xml') {
        $cid = $cml->id();
        break;
      }
    }
    if ($cid) {
      $data = \Drupal::service('cmlapi.parser_offers')->parseArray();
      foreach ($data['price'] as $key => $value) {
        $prices[$key] = $value;
      }
    }
    return $prices;
  }

  /**
   * Product Bundles.
   */
  private function getProductBundles() {
    $options = [];
    if ($this->moduleHandler->moduleExists('commerce_product')) {
      $enity_type = 'commerce_product';
      $bundles = $this->entityTypeBundle->getBundleInfo($enity_type);
      foreach ($bundles as $key => $value) {
        $options[$key] = $value['label'];
      }
    }
    return $options;
  }

  /**
   * Product Variation Bundles.
   */
  private function getProductVariationBundles() {
    $options = [];
    if ($this->moduleHandler->moduleExists('commerce_product')) {
      $enity_type = 'commerce_product_variation';
      $bundles = $this->entityTypeBundle->getBundleInfo($enity_type);
      foreach ($bundles as $key => $value) {
        $options[$key] = $value['label'];
      }
    }
    return $options;
  }

  /**
   * Taxonomy Bundles.
   */
  private function getTaxonomyBundles() {
    $vocabulares = [];
    if ($this->moduleHandler->moduleExists('taxonomy')) {
      $vocabulares = \Drupal::entityQuery('taxonomy_vocabulary')->accessCheck(FALSE)->execute();
    }
    return $vocabulares;
  }

  /**
   * Implements form validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  protected function getMigrationPlugins() {
    $manager = FALSE;
    $plugins = [];
    try {
      $manager = \Drupal::service('plugin.manager.migrate.source');
    }
    catch (\Exception $e) {
      return FALSE;
    }
    if ($manager) {
      foreach ($manager->getDefinitions() as $key => $source) {
        $plugins[$key] = "$key ({$source['provider'][0]})";
      }
    }
    return $plugins;
  }

  /**
   * Checking configs for prices.
   */
  public function checkPricesReady() {
    $storageTaxonomyVocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
    $vid = 'prices';
    if (is_null($storageTaxonomyVocabulary->load($vid))) {
      $vocabulary = $storageTaxonomyVocabulary->create([
        'name' => 'Price Types',
        'vid' => $vid,
      ])->save();
      $path = \Drupal::service('extension.list.module')->getPath('cmlmigrations');
      $config_path = "$path/assets/config/prices";
      $source = new FileStorage($config_path);
      \Drupal::service('config.installer')->installOptionalConfig($source);
      foreach (scandir($config_path) as $file) {
        if (substr($file, -4) != '.yml') {
          continue;
        }
        $cfg = substr($file, 0, -4);
        if ($cfg) {
          $data = $source->read($cfg);
          $config = \Drupal::service('config.factory')->getEditable($cfg);
          $config->setData($data)->save();
        }
      }
    }
  }

  /**
   * Creating variation process.
   */
  public function formVariationProcess(array $values) {
    $attributes = [];
    $stores = 0;
    $prices = 0;
    foreach ($values as $key => $value) {
      if (strpos($key, 'attribute_') !== FALSE) {
        $attributes[$key] = $value;
      }
      if ($key == 'stores') {
        $stores = $value;
      }
      if ($key == 'prices') {
        $prices = $value;
      }
    }
    $fieldArray = explode("\n", $values['variations-process']);
    $process = [];
    foreach ($fieldArray as $value) {
      if (!empty($value)) {
        $name = strstr($value, ':', TRUE);
        if ((array_key_exists($name, $attributes) && $attributes[$name]) || !array_key_exists($name, $attributes)) {
          $process[$name] = $name;
          if ($name == 'field_json_stores' && !$stores) {
            unset($process[$name]);
          }
          if ($name == 'field_json_prices' && !$prices) {
            unset($process[$name]);
          }
        }
      }
    }
    if ($stores) {
      $process['field_json_stores'] = 'field_json_stores';
    }
    if ($prices) {
      $process['field_json_prices'] = 'field_json_prices';
    }
    foreach ($attributes as $key => $value) {
      if (!array_key_exists($key, $process) && $value) {
        $process[$key] = $key;
      }
    }
    $fieldValues = Yaml::dump($process, 4);
    return $fieldValues;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cmlmigrations.settings');
    $timeout = trim($form_state->getValue('timeout'));
    if (!is_numeric($timeout)) {
      $timeout = 60;
    }
    $timeout2 = trim($form_state->getValue('timeout-quick-run'));
    if (!is_numeric($timeout2)) {
      $timeout2 = 60;
    }
    $config
      ->set('product', $form_state->getValue('product'))
      ->set('variation', $form_state->getValue('variation'))
      ->set('vocabulary', $form_state->getValue('vocabulary'))
      ->set('stores', $form_state->getValue('stores'))
      ->set('prices', $form_state->getValue('prices'))
      ->set('timeout', $timeout)
      ->set('timeout-quick-run', $timeout2)
      ->set('drush', $form_state->getValue('drush'))
      ->set('plugin-product-fetch', $form_state->getValue('plugin-product-fetch'))
      ->set('plugin-product-debug', $form_state->getValue('plugin-product-debug'))
      ->set('plugin-variation-fetch', $form_state->getValue('plugin-variation-fetch'))
      ->set('plugin-variation-debug', $form_state->getValue('plugin-variation-debug'))
      ->save();

    $catalog = $this->config('migrate_plus.migration.cml_taxonomy_catalog');
    if ($catalog->get('process')) {
      $catalog
        ->set('source', ['plugin' => $form_state->getValue('catalog-source')])
        ->set('process', Yaml::parse($form_state->getValue('catalog-process')))
        ->save();
    }
    $product = $this->config('migrate_plus.migration.cml_product');
    if ($product->get('process')) {
      $product
        ->set('source', ['plugin' => $form_state->getValue('product-source')])
        ->set('process', Yaml::parse($form_state->getValue('product-process')))
        ->save();
    }
    if ($form_state->getValue('prices')) {
      $this->checkPricesReady();
    }
    $variation = $this->config('migrate_plus.migration.cml_product_variation');
    if ($variation->get('process')) {
      $variation
        ->set('source', ['plugin' => $form_state->getValue('variations-source')])
        ->set('process', Yaml::parse($this->formVariationProcess($form_state->getValues())))
        ->save();
    }
    $module_handler = \Drupal::moduleHandler();
    $module_handler->invokeAll('cache_flush');
    foreach (Cache::getBins() as $service_id => $cache_backend) {
      $cache_backend
        ->deleteAll();
    }
  }

}
