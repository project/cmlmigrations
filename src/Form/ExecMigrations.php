<?php

namespace Drupal\cmlmigrations\Form;

use Drupal\cmlmigrations\Service\ExecService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Exec Migrations.
 */
class ExecMigrations extends FormBase {

  //phpcs:disable
  protected ExecService $execService;
  protected AjaxResponse $response;
  //phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cmlmigrations.exec')
    );
  }

  /**
   * Form constructor.
   *
   * Drupal\cmlmigrations\Service\ExecServiceInterface $exec
   *   Exec service.
   */
  public function __construct(ExecService $exec) {
    $this->execService = $exec;
    $this->response = new AjaxResponse();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cmlmigrations_exec';
  }

  /**
   * AJAX Responce.
   */
  public function ajax($otvet) {
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#exec-results", "<pre>{$otvet}</pre>"));
    return $response;
  }

  /**
   * AJAX Import.
   */
  public function ajaxImport(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxImport\n";
    $otvet .= $this->execService->exec(FALSE, FALSE);
    return $this->ajax($otvet);
  }

  /**
   * AJAX Import.
   */
  public function ajaxImportNohup(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxImportNohup\n";
    $otvet .= $this->execService->exec(TRUE, FALSE);
    return $this->ajax($otvet);
  }

  /**
   * AJAX Update.
   */
  public function ajaxUpdate(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxUpdate\n";
    $otvet .= $this->execService->exec(FALSE, TRUE);
    return $this->ajax($otvet);
  }

  /**
   * AJAX Update.
   */
  public function ajaxUpdateNohup(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxUpdateNohup\n";
    $otvet .= $this->execService->exec(TRUE, TRUE);
    return $this->ajax($otvet);
  }

  /**
   * AJAX Update.
   */
  public function mapProducts() {
    $destids = \Drupal::database()
      ->select('migrate_map_cml_product', 'maps')
      ->fields('maps', ['destid1'])
      ->orderBy('maps.destid1')
      ->execute();
    if ($destids) {
      $destid = [];
      foreach ($destids as $row) {
        $destid[] = $row->destid1;
      }
    }
    return $destid;
  }

  /**
   * AJAX Update.
   */
  public function unpublishProducts() {
    $destid = $this->mapProducts();
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_product');
    $products = \Drupal::entityQuery("commerce_product")->accessCheck(FALSE)
      ->condition('status', 1)
      ->condition('product_id', $destid, 'IN')
      ->execute();
    $count = 0;
    foreach ($storage->loadMultiple($products) as $product) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      $count++;
      $variations = $product->getVariations();
      foreach ($variations as $variation) {
        $variation->set('status', 0)->save();
      }
      $product->set('status', 0)->save();
    }
    $query = \Drupal::database()
      ->update('migrate_map_cml_product')
      ->fields(['hash' => ''])
      ->execute();
    $query = \Drupal::database()
      ->update('migrate_map_cml_product_variation')
      ->fields(['hash' => ''])
      ->execute();
    return "Все товары ($count) сняты с публикации<br>";
  }

  /**
   * AJAX Update.
   */
  public function countProducts(array $form, FormStateInterface $form_state) {
    if ($form_state->getValues()['product'] == 0) {
      $this->response->addCommand(new HtmlCommand("#products-count", ''));
      return $this->response;
    }
    $destid = $this->mapProducts();
    $count = count($destid) ?? 0;
    $this->response->addCommand(new HtmlCommand("#products-count", "$count товаров"));
    return $this->response;
  }

  /**
   * AJAX Update.
   */
  public function mapCatalog() {
    $destids = \Drupal::database()
      ->select('migrate_map_cml_taxonomy_catalog', 'maps')
      ->fields('maps', ['destid1'])
      ->orderBy('maps.destid1')
      ->execute();
    if ($destids) {
      $destid = [];
      foreach ($destids as $row) {
        $destid[] = $row->destid1;
      }
    }
    return $destid;
  }

  /**
   * AJAX Update.
   */
  private function unpublishCatalog() {
    $destid = $this->mapCatalog();
    $revisions = \Drupal::database()
      ->select('taxonomy_term_data', 'terms')
      ->fields('terms', ['revision_id'])
      ->condition('terms.tid', $destid, 'IN')
      ->execute();
    if ($revisions) {
      $revisionids = [];
      foreach ($revisions as $row) {
        $revisionids[] = $row->revision_id;
      }
    }
    $query = \Drupal::database()->update('taxonomy_term_field_data')
      ->fields(['status' => 0])
      ->condition('tid', $destid, 'IN')
      ->execute();
    $count = $query;
    $query = \Drupal::database()->update('taxonomy_term_field_revision')
      ->fields(['status' => 0])
      ->condition('revision_id', $revisionids, 'IN')
      ->execute();

    // \Drupal::service('cache_tags.invalidator')->invalidateTags(['taxonomy_term_list:catalog']);
    $query = \Drupal::database()
      ->update('migrate_map_cml_taxonomy_catalog')
      ->fields(['hash' => ''])
      ->execute();
    return "Все категории ($count) сняты с публикации<br>";
  }

  /**
   * AJAX Update.
   */
  public function countCategories(array $form, FormStateInterface $form_state) {
    if ($form_state->getValues()['category'] == 0) {
      $this->response->addCommand(new HtmlCommand("#categories-count", ''));
      return $this->response;
    }
    $destid = $this->mapCatalog();
    $count = count($destid) ?? 0;
    $this->response->addCommand(new HtmlCommand("#categories-count", "$count категорий"));
    return $this->response;
  }

  /**
   * AJAX Update.
   */
  public function ajaxUnpublish(array $form, FormStateInterface $form_state) {
    $confirm = $form['unpublish']['actions']['confirm']['#value'];
    $answer = '';
    if (!$confirm) {
      $answer = "<span style='color:red;font-size:large;'>Подтвердите свое действие!</span>";
    }
    else {
      if ($form['unpublish']['category']['#value']) {
        $answer .= $this->unpublishCatalog();
      }
      if ($form['unpublish']['product']['#value']) {
        $answer .= $this->unpublishProducts();
      }
      $answer = !empty($answer) ? $answer : "<span style='color:red;font-size:large;'>Вы не выбрали что снимать с публикации!</span>";
      $module_handler = \Drupal::moduleHandler();
      $module_handler
        ->invokeAll('cache_flush');
      foreach (Cache::getBins() as $service_id => $cache_backend) {
        $cache_backend
          ->deleteAll();
      }
    }
    $this->response->addCommand(new HtmlCommand("#confirm-results", $answer));
    return $this->response;
  }

  /**
   * AJAX Test Exec.
   */
  public function ajaxTestExec(array $form, FormStateInterface $form_state) {
    $otvet = "ajaxTestExec\n\n";
    $otvet .= $this->execService->execTest();
    return $this->ajax($otvet);
  }

  /**
   * AJAX Test Drush.
   */
  public function ajaxTestDrush(array $form, FormStateInterface $form_state) {
    $otvet = "Test Drush:\n\n";
    $otvet .= $this->execService->drushTest();
    return $this->ajax($otvet);
  }

  /**
   * AJAX Test Nohup.
   */
  public function ajaxTestNohup(array $form, FormStateInterface $form_state) {
    $otvet = "Test Nohup:\n\n";
    $otvet .= $this->execService->nohupTest();
    return $this->ajax($otvet);
  }

  /**
   * Button template.
   */
  public function ajaxButton($title, $callback) {
    return [
      '#type' => 'submit',
      '#value' => $title,
      '#ajax'   => [
        'callback' => $callback,
        'effect'   => 'fade',
        'progress' => ['type' => 'throbber', 'message' => ""],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $form_state->setCached(FALSE);

    $form['info'] = [
      '#markup' => '<h3>Запустить:</h3>',
    ];

    // Exec.
    $form['run'] = [
      '#type' => 'details',
      '#title' => $this->t('Run migrations'),
      '#open' => TRUE,
      'actions' => [
        '#type' => 'actions',
        'import' => $this->ajaxButton('Import', '::ajaxImport'),
        'import-nohup' => $this->ajaxButton('Import Nohup', '::ajaxImportNohup'),
        'update' => $this->ajaxButton('Update', '::ajaxUpdate'),
        'update-nohup' => $this->ajaxButton('Update Nohup', '::ajaxUpdateNohup'),
      ],
    ];
    // Unpublish.
    $form['unpublish'] = [
      '#type' => 'details',
      '#title' => $this->t('Unpublish'),
      '#open' => FALSE,
      'category' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Categories'),
        '#ajax' => [
          'callback' => '::countCategories',
          'event' => 'change',
        ],
        '#suffix' => '<div id="categories-count"></div>',
      ],
      'product' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Products'),
        '#ajax' => [
          'callback' => '::countProducts',
          'event' => 'change',
        ],
        '#suffix' => '<div id="products-count"></div>',
      ],
      'actions' => [
        '#type' => 'actions',
        'unpublish' => $this->ajaxButton('Снять с публикации', '::ajaxUnpublish'),
        'confirm' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Confirm unpublish'),
          '#suffix' => '<div id="confirm-results"></div>',
        ],
      ],
    ];
    // Test.
    $form['test'] = [
      '#type' => 'details',
      '#title' => $this->t('Test Environment'),
      '#open' => FALSE,
      'actions' => [
        '#type' => 'actions',
        'test-exec' => $this->ajaxButton('Test Exec', '::ajaxTestExec'),
        'test-drush' => $this->ajaxButton('Test Drush', '::ajaxTestDrush'),
        'test-nohup' => $this->ajaxButton('Test Nohup', '::ajaxTestNohup'),
      ],
    ];
    $form['#suffix'] = '<div id="exec-results"></div>';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
