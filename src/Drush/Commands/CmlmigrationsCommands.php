<?php

namespace Drupal\cmlmigrations\Drush\Commands;

use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class CmlmigrationsCommands extends DrushCommands {

  /**
   * Run cmlmigrations.
   *
   * @command cmlmigrations
   * @usage cmlmigrations
   */
  #[CLI\Command(name: 'cmlmigrations', aliases: ['cmlmigrations'])]
  #[CLI\Usage(name: 'cmlmigrations', description: 'Run cmlmigrations.')]
  public function cmlmigrationsRun() {
    // $config = \Drupal::config('cmlmigrations.settings');
    $this->output()->writeln("cmlmigrations:: RUN");
    $lock = \Drupal::lock();
    $delay = 5;
    if (!$lock->acquire('cmlmigrations')) {
      \Drupal::logger('cmlmigrations')->notice("Lock!! await {$delay}sec lock availability");
      while ($lock->wait('cmlmigrations', $delay)) {
        \Drupal::logger('cmlmigrations')->notice("Drush await {$delay}sec lock availability");
      };
      $lock->acquire('cmlmigrations');
    }
    \Drupal::logger('cmlmigrations')->notice("lock:: set");
    \Drupal::service('cmlmigrations.migrate')->import();
    \Drupal::logger('cmlmigrations')->notice("lock:: release");
    $lock->release('cmlmigrations');
  }

}
