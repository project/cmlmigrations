<?php

namespace Drupal\cmlmigrations\Hook;

use Drupal\Component\Serialization\Json;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\commerce_price\Price;

/**
 * @file
 * Contains \Drupal\cmlmigrations\Hook\CommerceProductVariationPresave.
 */

/**
 * Hook presave.
 */
class CommerceProductVariationPresave {

  /**
   * Hook.
   */
  public static function hook($variation) {
    if ($variation->hasField('field_json_stores') && !empty($variation->field_json_stores->value)) {
      $stores = JSON::decode($variation->field_json_stores->value);
      if (!empty($stores)) {
        $mapStores = self::queryMap('migrate_map_cml_taxonomy_stores');
        $paragraphs = [];
        foreach ($stores as $key => $value) {
          if (!empty($mapStores[$key])) {
            $paragraphs[] = self::hasStore($mapStores[$key], $value, $variation->id());
          }
        }
        $variation->set('field_stocks', $paragraphs);
      }
    }
    if ($variation->hasField('field_json_prices') && !empty($variation->field_json_prices->value)) {
      $prices = JSON::decode($variation->field_json_prices->value);
      if (!empty($prices)) {
        $mapPrices = self::queryMap('migrate_map_cml_taxonomy_prices');
        $paragraphs = [];
        foreach ($prices as $key => $value) {
          if (!empty($mapPrices[$key])) {
            $price = new Price($value['number'], $value['currency_code']);
            $paragraphs[] = self::hasPrice($mapPrices[$key], $price, $variation->id());
          }
        }
        $variation->set('field_prices', $paragraphs);
      }
    }
  }

  /**
   * HasStore.
   */
  private static function hasStore($store_id, $quantity, $variationId) {
    $result = FALSE;
    $storage = \Drupal::entityTypeManager()->getStorage('paragraph');
    $ids = $storage
      ->getQuery()
      ->condition('status', 1)
      ->condition('type', 'stores')
      ->condition('parent_type', 'commerce_product_variation')
      ->condition('parent_field_name', 'field_stores')
      ->condition('parent_id', $variationId)
      ->condition('field_stores_store', $store_id)
      ->accessCheck(TRUE)
      ->execute();
    if ($ids) {
      foreach ($storage->loadMultiple($ids) as $paragraph) {
        $paragraph->set('field_stores_stock', $quantity);
        $paragraph->save();
      }
    }
    else {
      $paragraph = Paragraph::create([
        'type' => 'stores',
        'parent_id' => $variationId,
        'parent_type' => 'commerce_product_variation',
        'parent_field_name' => 'field_stores',
        'field_stores_stock' => $quantity,
        'field_stores_store' => [
          'target_id' => $store_id,
        ],
      ]);
      $paragraph->save();
    }
    $result = [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
    return $result;
  }

  /**
   * HasPrice.
   */
  private static function hasPrice($price_id, $price, $variationId) {
    $result = FALSE;
    $storage = \Drupal::entityTypeManager()->getStorage('paragraph');
    $ids = $storage
      ->getQuery()
      ->condition('status', 1)
      ->condition('type', 'prices')
      ->condition('parent_type', 'commerce_product_variation')
      ->condition('parent_field_name', 'field_prices')
      ->condition('parent_id', $variationId)
      ->condition('field_prices_types', $price_id)
      ->accessCheck(TRUE)
      ->execute();
    if ($ids) {
      foreach ($storage->loadMultiple($ids) as $paragraph) {
        $paragraph->set('field_prices_price', $price);
        $paragraph->save();
      }
    }
    else {
      $paragraph = Paragraph::create([
        'type' => 'prices',
        'parent_id' => $variationId,
        'parent_type' => 'commerce_product_variation',
        'parent_field_name' => 'field_prices',
        'field_prices_price' => $price,
        'field_prices_types' => [
          'target_id' => $price_id,
        ],
      ]);
      $paragraph->save();
    }
    $result = [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
    return $result;
  }

  /**
   * Query MAP.
   */
  private static function queryMap($table, $subs = FALSE) {
    $data = [];
    $db = \Drupal::database();
    if (!$db->schema()->tableExists($table)) {
      return [];
    }
    $query = $db->select($table, 'map')->fields('map', [
      'sourceid1',
      'destid1',
    ]);
    $res = $query->execute();
    if ($res) {
      foreach ($res as $key => $row) {
        $data[$row->sourceid1] = $row->destid1;
      }
    }
    return $data;
  }

}
