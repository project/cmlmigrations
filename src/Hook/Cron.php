<?php

namespace Drupal\cmlmigrations\Hook;

/**
 * Hook Cron Staff.
 */
class Cron {

  /**
   * Hook.
   */
  public static function hook() {
    $clear = \Drupal::service('cmlmigrations.clear')->clear();
    if ($clear['migrations'] && $clear['cmlmigrations']) {
      \Drupal::service('cmlmigrations.migrate')->import();
    }
  }

}
